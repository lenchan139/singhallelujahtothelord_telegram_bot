import { BotHandler, ChatType} from "./botHandler";
import { resolve } from "path";
import * as express from 'express'
let botHandler = new BotHandler()
let botUsername = "@singHallelujahToTheLord_bot"
let singFileId = "CQADBQADlQADGKtRVN47EgMxQtZMAg"
botHandler.registerOnText(/\/start/, (message=>{
    // on /start
    let chatId = message.chat.id
    botHandler.sendMessage(chatId, "真主偉大！")
}))
botHandler.onMessage((message, metadata)=>{
    console.debug(message)
})
botHandler.registerOnText(/\/sing/, message=>{
    let chatId = message.chat.id 
    if(message.chat.type == ChatType.group){
        if(message.text.includes(botUsername)) sing(chatId)
    }else if(message.chat.type == ChatType.private){
        sing(chatId)
    } 
})
botHandler.registerOnText(/^(?!\/.*$).*/, message=>{
    console.debug(message)
    // on non start with /
    let chatId = message.chat.id
    if(message.chat.type == ChatType.group || message.chat.type == ChatType.supergroup){
        if(message.text.includes(botUsername)) sing(chatId)
    }else if(message.chat.type == ChatType.private){
        sing(chatId)
    } 
})
console.debug("Load Completed!")
async function sing(chatId: string){
    botHandler.sendAudio(chatId, singFileId)
    await delay(500)
    botHandler.sendMessage(chatId, "Sing Hallelujah to the Lord!")
}
async function delay(ms:number){
    return new Promise(resolve=>{
        setTimeout(()=>{
            resolve()
        },ms)
    })
}

// mount express 

const expressApp = express()

const port = process.env.PORT || 3000
expressApp.get('/', (req, res) => {
  res.send('___')
})
expressApp.listen(port, () => {
  console.log(`Listening on port ${port}`)
})