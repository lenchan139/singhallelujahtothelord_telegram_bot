import * as TelegramBot from 'node-telegram-bot-api'
export class BotHandler{
    private token = process.env.telegramSingBotToken;
    private bot : TelegramBot;
    
    
    public constructor(){
        this.bot = new TelegramBot(this.token, {polling: true})
    }
    async onMessage(callback:(message: TelegramBot.Message, metadata: TelegramBot.Metadata)=>void){
         this.bot.on('message', callback)
    }
    async sendMessage(chatId: string | number, message: string, options?:TelegramBot.SendMessageOptions): Promise<TelegramBot.Message>{
        return await this.bot.sendMessage(chatId, message, options)
    }
    async sendAudio(chatId:string, fileId:string, options?:TelegramBot.SendAudioOptions){
        return await this.bot.sendAudio(chatId, fileId, options)
    }
    registerOnText(text:RegExp, callback: (message)=>void){
        return this.bot.onText(text, callback)
    }
}
export enum ChatType{
    private = "private",
    group = "group",
    supergroup = "supergroup"
}